from cmath import inf
import os
import re


class Parser:
    def __init__(self, folder):
        self.folder = folder
        self.unique_users = dict()
        self.unique_requests = dict()

    def __clear_id__(self, id_str):
        '''
        @id_str string in form "[0-9]+"
        '''

        if id_str == '':
            return None
        splitted = id_str.split('"')
        clean = int(splitted[1])
        return clean

    def __clear_request__(self, request):
        ''''
        @request second half of request where user id is not included
        returns (times of item requests, zip in form (item_id, status))
        '''
        #Getting all item_ids
        regex_id = re.findall(r'"[0-9]+"', request)
        ids_in_request = []
        for item in regex_id:
            ids_in_request.append(int(item[1:-1]))
        
        #getting all times 
        regex_time = re.findall(
            r'"[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]T[0-9:.+]*"', request)
        times_in_request = []
        for item in regex_time:
            #use just time without +... and date
            times_in_request.append(item[1:-1].split('T')[1].split("+")[0])

        #getting all status of requests ids
        regex_status = re.findall(
            r'"similarInJsonList"]|"NotReadyYet"]|"NotEnoughSimilarsInStock"]', request)
        status = []
        for item in regex_status:
            status.append(item.split('"')[1])
        #link item_ids with statuses
        zipped_id_with_status = list(zip(ids_in_request, status))
        
        return times_in_request, zipped_id_with_status
        
    def __avg_time__(self, time_list):
        '''
        @time_list - list with time in form HH:MM:SS
        return avg gap between requests for single user
        ''''

        seconds_list = []
        if len(time_list) == 0:
            return 0
        min = inf
        for time in time_list:
            splitted_time = time.split(":")
            minutes = float(splitted_time[1])
            secs = float(splitted_time[2])
            seconds_list.append(minutes * 60 + secs)
            if min > seconds_list[-1]:
                min = seconds_list[-1]
        
    
        seconds_list = list(map(lambda x: x - min, seconds_list))
        time_sum = sum(seconds_list)
        return time_sum / len(time_list)

    def __median_time__(self, time_list):
        '''
        @time_list - list with time in form HH:MM:SS
        return median gap between requests for single user
        ''''

        seconds_list = []
        if len(time_list) == 0:
            return 0

        for time in time_list:
            splitted_time = time.split(":")
            minutes = float(splitted_time[1])
            secs = float(splitted_time[2])
            seconds_list.append(minutes * 60 + secs)
        seconds_list = sorted(seconds_list)
        start = seconds_list[0]
        seconds_list = list(map(lambda x: x - start, seconds_list))
        if len(seconds_list) % 2 == 0:
            idx = int(len(seconds_list) / 2)
            return (seconds_list[idx] + seconds_list[idx - 1])/2
        else:
            idx = int(len(seconds_list) / 2)
            return seconds_list[idx]

    def parse(self):
        '''
        parse data from text format and fill unique user and unique item
        '''
        files = os.listdir(self.folder)
        for file in files:
            items = []
            with open(self.folder + '/' + file, 'r') as data:
                raw = data.readlines()
                items = re.split("{|}", raw[0])
            for i in range(1, len(items), 2):
                id = self.__clear_id__(items[i])
                
                if id is None:
                    continue
                
                times, zipped = self.__clear_request__(items[i+1])
                for id_zip, status in zipped:
                    add = 0
                    if status == "similarInJsonList": #count items with similarInJsonList status 
                        add = 1
                    if id_zip in self.unique_requests:
                        self.unique_requests[id_zip] += add 
                    else:
                        self.unique_requests[id_zip] = add
                    
                avg_time = self.__avg_time__(times)
                median = self.__median_time__(times)
                

                if id in self.unique_users:
                    self.unique_users[id].append([file, avg_time, median])
                else:
                    self.unique_users[id] = [[file, avg_time, median]]


if __name__ == '__main__':
    parser = Parser("data")
    parser.parse()
    print("Unique users: " + str(len(parser.unique_users)))
    print("Unique items requests: " + str(len(parser.unique_requests)))
    
    with open("user_times.txt", 'w') as F:
        F.write("User : ______" + " Time: [file, avg in seconds, median in seconds]\n")
        for key, val in parser.unique_users.items():
            F.write("User : " + str(key) + " Time: " + str(val) + "\n")
    
    max = [None, 0]
    for key, val in parser.unique_requests.items():
        if val > max[1]:
            max[1] = val
            max[0] = key

    print("maximum number of requests per a single id -> id = " + str(max[0]) + " requests = " + str(max[1]))
